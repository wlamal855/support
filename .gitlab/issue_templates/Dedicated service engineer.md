Title: Dedicated Service Engineer - CompanyName

#### Reseller overview

+ Company name:

+ Company email domain:

+ Primary Contact:

+ Timezone:

+ Salesforce account:

+ Sales contact:

_(mark issue as confidential, and assign to the Support Lead)_


#### Support actions

- [ ] **Support Lead**: Assign Service Engineer to reseller. Ping them in this issue.
- [ ] **Support Lead**: Update Dedicated Service Engineer [spreadsheet](https://docs.google.com/spreadsheets/d/1fCQ3yTbu6y2uKMM4IIEljzAZgHX2FFeG2y9XwWy7G-g/edit).
- [ ] **Support Lead/Zendesk Admin**: Create DSE triggers in ZenDesk - [more information](https://about.gitlab.com/handbook/support/knowledge-base/categories/zendesk/create_dse_trigger.html)
- [ ] **Assigned SE**: Create a [new Zendesk ticket](https://gitlab.zendesk.com/agent/tickets/new/2) using the resellers contact email as the "Requester". Apply the following template as the "Description", personalizing where appropriate.

> Title: GitLab - Dedicated Service Engineer for {{RESELLERNAME}}
>
> Hi {{CONTACTNAME}},
>
> As part of the relationship between you and GitLab, I'll be serving as your dedicated service engineer. This means that all of your support requests will be handled by myself, and I'll be happy to coordinate two training workshops for you as well.
>
> **Contacting support**
>
> Please use the following contact information when reaching out to GitLab for support.
>
>  + File your support requests via the web form at https://support.gitlab.com
>  + You can also email for general support issues/questions: {{SUBSCRIBERS EMAIL}}
>  + Please don't forget to gather all relevant data regarding the issue you are facing before submitting the ticket, see https://about.gitlab.com/handbook/resellers/#technical-support for more details.
>
> In each case, tickets will be automatically assigned to me.
>
> **Training workshops**
>
> You can see a list of our offered training workshops here https://about.gitlab.com/training/
>
> Please let me know which two training workshops would best suit your needs and what date & time is best to deliver these.
>
>

- [ ] **Assigned SE**: Schedule two training workshops with the customer, arrange these via the above Zendesk ticket or via a video call.
